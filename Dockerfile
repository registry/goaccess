# Builds a goaccess image from the current working directory:
FROM alpine:edge

ARG build_deps="build-base ncurses-dev autoconf automake git gettext-dev geoip-dev wget"
ARG runtime_deps="tini ncurses libintl gettext openssl-dev geoip"

RUN apk update && \
    apk add -u $runtime_deps $build_deps && \
    wget -q http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz && \
    gunzip GeoIP.dat.gz && \
    mkdir -p /usr/share/GeoIP && \
    mv GeoIP.dat /usr/share/GeoIP/GeoIP.dat && \
    git clone https://github.com/allinurl/goaccess && \
    cd /goaccess && \
    autoreconf -fiv && \
    ./configure --enable-utf8 --with-openssl --enable-geoip=legacy && \
    make && \
    make install && \
    apk del $build_deps && \
    rm -rf /var/cache/apk/* /tmp/goaccess/* /goaccess

WORKDIR /goaccess

VOLUME /srv/data
VOLUME /srv/logs
VOLUME /srv/report
EXPOSE 7890

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["goaccess", "--no-global-config", "--config-file=/srv/data/goaccess.conf"]
